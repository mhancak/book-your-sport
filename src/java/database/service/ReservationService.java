/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service;

import database.entities.ReservationE;
import database.entities.SubPlayersE;
import java.util.Date;
import java.util.List; 

/**
 *
 * @author mato
 */
public interface ReservationService {
    
    /**
     * Vytvorenie novej rezervacie v databaze.
     * @param reservation 
     */
    public void createReservation(ReservationE reservation);
    
    /**
     * Odstranenie rezervacie z databazy pod ID.
     * @param reservationId 
     */
    public boolean deleteReservation(long reservationId);
    
    /**
     * Metoda vrati referenciu na rezervaciu z databazy pod ID rezervacie.
     * @param reservationId
     * @return 
     */
    public ReservationE getReservationById(long reservationId);
    
    /**
     * Metoda vrati zoznam rezervacii pre dany den.
     * @param date
     * @return 
     */
    public List<ReservationE> getReservationsByResDate(Date date);
    
    /**
     * Modifikacia poctu potrebnych hracov pre danu rezervaciu.
     * @param reservationId
     * @param newReqPlayers 
     */
    public void alterReqPlayers(long reservationId, int newReqPlayers);
            
    /**
     * Modifikacia casu zaciatku danej rezervacie.
     * @param reservationId
     * @param newStartTime 
     */
    public void alterStartTime(long reservationId, Date newStartTime);
    
    /**
     * Modifikacia casu konca danej rezervacie.
     * @param reservationId
     * @param newEndTime 
     */
    public void alterEndTime(long reservationId, Date newEndTime);
    
    /**
     * Modifikacia datumu, na kedy je vytvorena rezervacia.
     * @param reservationId
     * @param newReservDate 
     */
    public void alterReservDate(long reservationId, Date newReservDate);
    
    /**
     * Modifikacia nahradnikov pre danu rezervaiu
     * @param reservationId
     * @param newSubPlayers 
     */
    public void alterSubPlayer(long reservationId, List<SubPlayersE> newSubPlayers);
    
    /**
     * Metoda vrati zoznam vytvorenych rezervacii pre dany sport.
     * @param sportId
     * @return 
     */
    public List<ReservationE> getReservationsBySportId(long sportId);
    
    /**
     * Metoda vrati rezervaciu na zaklade event ID
     * @param eventID
     * @return 
     */
    public ReservationE getReservationByEventID(String eventID);
    
    /**
     * Metoda vrati zoznam vytvorenych rezervacii pre dane sportovisko.
     * @param sportFacilityId
     * @return 
     */     
    public List<ReservationE> getReservationsBySportFacilityId(long sportFacilityId);
    
    /**
     * Metoda vrati zoznam rezerevacii z databazy zacinajucich na pozicii {@code at}
     * a maximalne {@max} vysledkov na dopyt.
     * 
     * @param at
     * @param max
     * @return 
     */
    public List<ReservationE> getReservations(int at, int max);
    
    /**
     * Celkovy pocet rezervacii v databaze.
     * 
     * @return 
     */
    public Long countReservationsTotal();
    
    /**
     * Metoda vrati vsetky rezervacie v databaze.
     * @return 
     */
    public List<ReservationE> getAllReservations();
    
    /**
     * Metoda vrati vsetky rezervacie zacinajuce "titulkou" {@code title}. Metoda
     * je case insensitive.
     * @param title
     * @return 
     */
    public List<ReservationE> getReservationsStartByTitle(String title);
    
    /**
     * Metoda vrati vsetky rezervacie zacinajuce "emailom" {@code email}. Metoda
     * je case insensitive.
     * @param title
     * @return 
     */
    public List<ReservationE> getReservationsStartByUserEmail(String email);
    
    /**
     * Metoda vrati vsetky rezervacie zacinajuce "nazvom sportu" {@code sportName}. 
     * Metoda je case insensitive.
     * @param sportName
     * @return 
     */
    public List<ReservationE> getReservationsStartBySportName(String sportName);
    
    /**
     * Metoda vrati vsetky rezervacie zacinajuce sportovym zariadenim (jeho menom) 
     * {@code sfName}. Metoda je case insensitive.
     * @param sfName
     * @return 
     */
    public List<ReservationE> getReservationsStartBySportFacilityName(String sfName);
}
