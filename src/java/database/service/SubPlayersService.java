/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service;

import database.entities.SubPlayersE;

/**
 *
 * @author mato
 */
public interface SubPlayersService {
    
    /**
     * Metoda vrati nahradnika podla id.
     * @param subPlayerId 
     */
    public SubPlayersE getSubPlayersById(long subPlayerId);
    
    /**
     * Metoda vytvori zaznam s nahradnikmi v databaze.
     * @param newSubPlayer 
     */
    public void createSubPlayers(SubPlayersE newSubPlayer);
    
    /**
     * Odstranenie zaznamu s nahradnikmi z databazy.
     * @param subPlayerId 
     */
    public void deleteSubPlayers(long subPlayerId);
    
    /**
     * Modifikacia poctu nahradnikov pre dane id.
     * @param subPlayerId
     * @param newCount 
     */
    public void alterSubPlayersCount(long subPlayerId, int newCount);    
    
}
