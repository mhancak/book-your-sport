/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service.impl;

import database.entities.SubPlayersE;
import database.service.SubPlayersService;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;

/**
 *
 * @author mato
 */
@ManagedBean(name="subPlayersService")
@SessionScoped
public class SubPlayersBean implements SubPlayersService{
    @ManagedProperty(value="#{jpaResourceBean}")
    private JPAResourceBean jpaResourceBean;
    
    @Override
    public SubPlayersE getSubPlayersById(long subPlayerId) {
        EntityManager em = getJpaResourceBean().getEMF().createEntityManager();
        try{
            return em.find(SubPlayersE.class, subPlayerId);
        }
        finally{
            em.close();
        }
    }

    @Override
    public void createSubPlayers(SubPlayersE newSubPlayer) {
        EntityManager em = getJpaResourceBean().getEMF().createEntityManager();
        try{
            em.getTransaction().begin();            
            em.persist(newSubPlayer);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    @Override
    public void deleteSubPlayers(long subPlayerId) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            SubPlayersE subPlayer = em.find(SubPlayersE.class, subPlayerId);        
            em.remove(subPlayer);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    @Override
    public void alterSubPlayersCount(long subPlayerId, int newCount) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            SubPlayersE subPlayer = em.find(SubPlayersE.class, subPlayerId);
            subPlayer.setCount(newCount);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    /**
     * @return the jpaResourceBean
     */
    public JPAResourceBean getJpaResourceBean() {
        return jpaResourceBean;
    }

    /**
     * @param jpaResourceBean the jpaResourceBean to set
     */
    public void setJpaResourceBean(JPAResourceBean jpaResourceBean) {
        this.jpaResourceBean = jpaResourceBean;
    }
    
}
