/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service.impl;

import database.entities.ReservationE;
import database.entities.SubPlayersE;
import database.service.ReservationService;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;

/**
 *
 * @author mato
 */
@ManagedBean(name = "reservationService")
@SessionScoped
public class ReservationBean implements ReservationService {

    @ManagedProperty(value = "#{jpaResourceBean}")
    private JPAResourceBean jpaResourceBean;

    @Override
    public void createReservation(ReservationE reservation) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(reservation);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Override
    public boolean deleteReservation(long reservationId) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try {
            em.getTransaction().begin();
            ReservationE reservation = em.find(ReservationE.class, reservationId);
            em.remove(reservation);
            em.getTransaction().commit();
        } catch (Exception ex) {
            return false;
        } finally {
            em.close();
        }
        return true;
    }

    @Override
    public ReservationE getReservationById(long reservationId) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try {
            return em.find(ReservationE.class, reservationId);
        } finally {
            em.close();
        }
    }

    @Override
    public List<ReservationE> getReservationsByResDate(Date date) {
        EntityManager em;
        if (jpaResourceBean != null) {
            em = jpaResourceBean.getEMF().createEntityManager();
        } else {
            em = new JPAResourceBean().getEMF().createEntityManager();
        }
        
        Calendar calendar = Calendar.getInstance();
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.setTime(date);
        List<ReservationE> reservations = em.createQuery("SELECT r FROM ReservationE r").getResultList();
        List<ReservationE> reservationsByDate = new ArrayList<ReservationE>();
        
        for(ReservationE reservation : reservations){
            calendar.setTime(reservation.getStartDate());
            if((calendar.get(1) == dateCalendar.get(1)) && (calendar.get(2) == dateCalendar.get(2)) && (calendar.get(5) == dateCalendar.get(5))){
                reservationsByDate.add(reservation);
            }
        }
        return reservationsByDate;
    }

    @Override
    public void alterReqPlayers(long reservationId, int newReqPlayers) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try {
            em.getTransaction().begin();
            ReservationE sportFacility = em.find(ReservationE.class, reservationId);
            sportFacility.setReqPlayers(newReqPlayers);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Override
    public void alterStartTime(long reservationId, Date newStartTime) {
        ReservationE reservation;
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try {
            em.getTransaction().begin();
            reservation = em.find(ReservationE.class, reservationId);
            reservation.setEndDate(newStartTime);
            em.getTransaction().commit();
            em.refresh(reservation);
        } finally {
            em.close();
        }
    }

    @Override
    public void alterEndTime(long reservationId, Date newEndTime) {
        ReservationE reservation;
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try {
            em.getTransaction().begin();
            reservation = em.find(ReservationE.class, reservationId);
            reservation.setEndDate(newEndTime);
            em.getTransaction().commit();
            em.refresh(reservation);
        } finally {
            em.close();
        }
    }

    @Override
    public void alterReservDate(long reservationId, Date newReservDate) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try {
            em.getTransaction().begin();
            ReservationE reservation = em.find(ReservationE.class, reservationId);
            reservation.setStartDate(newReservDate);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Override
    public void alterSubPlayer(long reservationId, List<SubPlayersE> newSubPlayers) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try {
            em.getTransaction().begin();
            ReservationE sportFacility = em.find(ReservationE.class, reservationId);
            sportFacility.setSubPlayers(newSubPlayers);
            em.getTransaction().commit();
        } finally {
            em.close();
        }
    }

    @Override
    public List<ReservationE> getReservationsBySportId(long sportId) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();

        return em.createQuery(
                "SELECT r FROM ReservationE r JOIN r.sport s WHERE s.id = :sportID")
                .setParameter("sportID", sportId)
                .getResultList();
    }

    @Override
    public ReservationE getReservationByEventID(String eventID) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();

        return (ReservationE) em.createQuery(
                "SELECT r FROM ReservationE r WHERE r.id = :eventID")
                .setParameter("eventID", eventID)
                .getSingleResult();
    }

    @Override
    public List<ReservationE> getReservationsBySportFacilityId(long sportFacilityId) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();

        System.out.println("-----Dtabase getting reservations-----");

        return em.createQuery(
                "SELECT r FROM ReservationE r JOIN r.sportFacility s WHERE s.id = :sportfacilityID")
                .setParameter("sportfacilityID", sportFacilityId)
                .getResultList();
    }

    @Override
    public List<ReservationE> getReservations(int at, int max) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();

        return em.createQuery(
                "SELECT r FROM ReservationE r")
                .setFirstResult(at)
                .setMaxResults(max)
                .getResultList();
    }

    @Override
    public Long countReservationsTotal() {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();

        Long a = (Long) em.createQuery(
                "SELECT count(r) FROM ReservationE r")
                .getSingleResult();

        if (a == null) {
            a = new Long(0);
        }

        return a;
    }

    @Override
    public List<ReservationE> getAllReservations() {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();

        return em.createQuery(
                "SELECT r FROM ReservationE r")
                .getResultList();
    }

    @Override
    public List<ReservationE> getReservationsStartByTitle(String title) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();

        return em.createQuery(
                "SELECT r FROM ReservationE r WHERE LOWER(r.title) LIKE :tit")
                .setParameter("tit", title.toLowerCase() + "%") //% predstavuje lubovolny retazec
                .getResultList();
    }

    @Override
    public List<ReservationE> getReservationsStartByUserEmail(String usermail) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();

        return em.createQuery(
                "SELECT r FROM ReservationE r JOIN r.userE u WHERE LOWER(u.email) LIKE :usermail")
                .setParameter("usermail", usermail.toLowerCase() + "%") //% predstavuje lubovolny retazec
                .getResultList();
    }

    @Override
    public List<ReservationE> getReservationsStartBySportName(String sportName) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();

        return em.createQuery(
                "SELECT r FROM ReservationE r JOIN r.sport s WHERE LOWER(s.name) LIKE :sportname")
                .setParameter("sportname", sportName.toLowerCase() + "%") //% predstavuje lubovolny retazec
                .getResultList();
    }

    @Override
    public List<ReservationE> getReservationsStartBySportFacilityName(String sfName) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();

        return em.createQuery(
                "SELECT r FROM ReservationE r JOIN r.sportFacility s WHERE LOWER(s.name) LIKE :sfname")
                .setParameter("sfname", sfName.toLowerCase() + "%") //% predstavuje lubovolny retazec
                .getResultList();
    }

    /**
     * @return the jpaResourceBean
     */
    public JPAResourceBean getJpaResourceBean() {
        return jpaResourceBean;
    }

    /**
     * @param jpaResourceBean the jpaResourceBean to set
     */
    public void setJpaResourceBean(JPAResourceBean jpaResourceBean) {
        this.jpaResourceBean = jpaResourceBean;
    }
}
