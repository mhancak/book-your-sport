/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service.impl;

import database.entities.ReservationE;
import database.entities.SportsE;
import database.entities.UserE;
import database.service.UserService;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 *
 * @author mato
 */
@ManagedBean(name="userService")
@SessionScoped
public class UserBean implements UserService{
    @ManagedProperty(value="#{jpaResourceBean}")
    private JPAResourceBean jpaResourceBean;
    
    @Override
    public UserE getUserById(long id) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            return em.find(UserE.class, id);
        }
        finally{
            em.close();
        }
    }
    
    @Override
    public UserE getUserByEmail(String email) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            return (UserE) em.createQuery(
                "SELECT e FROM UserE e WHERE e.email LIKE :email")
                .setParameter("email", email)
                .getSingleResult();
        }
        catch(NoResultException e) {
            return null;
        }
    }
    
    @Override
    public void createUser(UserE user) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();            
            em.persist(user);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    @Override
    public void deleteUser(long userId) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            //najst pouzivatela s userId
            UserE user = em.find(UserE.class, userId);
            //odstranit ho z databazy
            em.remove(user);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    @Override
    public boolean existsEmail(String email) {        
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        
        Long a = (Long) em.createQuery(
            "SELECT count(e.email) FROM UserE e WHERE e.email LIKE :email")
            .setParameter("email", email)      
            .setMaxResults(1)
            .getSingleResult();
        
        return  (a.longValue()==0) ? false : true;   
    }

    @Override
    public void alterEmail(long userId, String newEmail) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            UserE user = em.find(UserE.class, userId);
            user.setEmail(newEmail);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    @Override
    public void alterFirstname(long userId, String newFirstname) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            UserE user = em.find(UserE.class, userId);
            user.setFirsname(newFirstname);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    @Override
    public void alterLastname(long userId, String newLastname) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            UserE user = em.find(UserE.class, userId);
            user.setLastname(newLastname);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    @Override
    public void alterPass(long userId, String newPass) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            UserE user = em.find(UserE.class, userId);
            user.setPasswd(newPass);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    @Override
    public void alterPhone(long userId, String newPhone) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            UserE user = em.find(UserE.class, userId);
            user.setPhone(newPhone);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    @Override
    public void alterSports(long userId, List<SportsE> newSports) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            UserE user = em.find(UserE.class, userId);
            user.setSports(newSports);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }
    
    @Override
    public void alterReservations(long userId, List<ReservationE> newReservations) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            UserE user = em.find(UserE.class, userId);
            user.setReservations(newReservations);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    @Override
    public void alterUser(long userId, UserE newDataFromUser) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            
            UserE user = em.find(UserE.class, userId);            
            user.setEmail(newDataFromUser.getEmail());
            user.setFirsname(newDataFromUser.getFirsname());
            user.setLastname(newDataFromUser.getLastname());
            user.setPasswd(newDataFromUser.getPasswd());
            user.setPhone(newDataFromUser.getPhone());
            user.setReservations(newDataFromUser.getReservations());
            user.setSports(newDataFromUser.getSports());
            user.setAdmin(newDataFromUser.isAdmin());
            
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    @Override
    public List<UserE> getUsersBySportId(long sportId) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        
        return em.createQuery(
            "SELECT e FROM UserE e JOIN e.sports s WHERE s.id = :sportID")
            .setParameter("sportID", sportId)
            .getResultList();
    }    

    @Override
    public void alterAdmin(long userId, boolean isAdmin) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            UserE user = em.find(UserE.class, userId);
            user.setAdmin(isAdmin);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }
    
    @Override
    public List<UserE> getAllAdmins() {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            return em.createQuery(
                "SELECT e FROM UserE e WHERE e.admin = :admin")
                .setParameter("admin", true)
                .getResultList();
                
        }
        catch(NoResultException e) {
            return null;
        }
    }
    
    /**
     * @return the jpaResourceBean
     */
    public JPAResourceBean getJpaResourceBean() {
        return jpaResourceBean;
    }

    /**
     * @param jpaResourceBean the jpaResourceBean to set
     */
    public void setJpaResourceBean(JPAResourceBean jpaResourceBean) {
        this.jpaResourceBean = jpaResourceBean;
    }

   
}
