/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service.impl;

import database.entities.SportsE;
import database.service.SportsService;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;

/**
 *
 * @author mato
 */
@ManagedBean(name="sportsService")
@SessionScoped
public class SportsBean implements SportsService{
    @ManagedProperty(value="#{jpaResourceBean}")
    private JPAResourceBean jpaResourceBean;
    
    @Override
    public void createSport(SportsE sport) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();            
            em.persist(sport);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }

    @Override
    public boolean deleteSport(long sportId) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            SportsE sport = em.find(SportsE.class, sportId);        
            em.remove(sport);
            em.getTransaction().commit();            
        }
        catch (Exception ex){
            return false;
        }
        finally{
            em.close();
        }
        return true;
    }

    @Override
    public SportsE getSportById(long sportId) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            return em.find(SportsE.class, sportId);
        }
        finally{
            em.close();
        }
    }

    @Override
    public List<SportsE> getAllSports() {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();        
        return em.createQuery("SELECT s FROM SportsE s").getResultList();
    }

    @Override
    public void alterSportName(long sportId, String newName) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        try{
            em.getTransaction().begin();
            SportsE sport = em.find(SportsE.class, sportId);
            sport.setName(newName);
            em.getTransaction().commit();
        }finally{
            em.close();
        }
    }
    
    @Override
    public boolean existsSportWithName(String sportName) {
        EntityManager em = jpaResourceBean.getEMF().createEntityManager();
        
        Long a = (Long) em.createQuery(
            "SELECT count(s.name) FROM SportsE s WHERE s.name LIKE :sportname")
            .setParameter("sportname", sportName)      
            .setMaxResults(1)
            .getSingleResult();
        
        return  (a.longValue()==0) ? false : true;   
    }

    /**
     * @return the jpaResourceBean
     */
    public JPAResourceBean getJpaResourceBean() {
        return jpaResourceBean;
    }

    /**
     * @param jpaResourceBean the jpaResourceBean to set
     */
    public void setJpaResourceBean(JPAResourceBean jpaResourceBean) {
        this.jpaResourceBean = jpaResourceBean;
    }
    
}
