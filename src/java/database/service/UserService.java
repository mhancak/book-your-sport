/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service;

import database.entities.ReservationE;
import database.entities.SportsE;
import database.entities.UserE;
import java.util.List;

/**
 *
 * @author mato
 */
public interface UserService {
    
    /** 
     * Metoda vrati objekt pouzivatela podla id. 
     * @param id
     */
    public UserE getUserById(long id);
    
    /**
     * Metoda vrati objekt pouzivatela podla emailu.
     * @param email
     * @return 
     */
    public UserE getUserByEmail(String email); 
    
    /**
     * Vytvorenie pouzivatela v databaze.
     * @param user 
     */
    public void createUser(UserE user);
    
    /**
     * Odstranenie pouzivatela s {@code userId} z databazy.
     * @param userId 
     */
    public void deleteUser(long userId);
    
    /**
     * Metoda zisti ci sa v databaze nachadza uzivatel s danou mailovou adresou.
     * @param email
     * @return true ak mail existuje pri nejakom pouzivatelovi.
     */
    public boolean existsEmail(String email);
    
    /**
     * Zmena emailu
     * @param userId id pouzivatela, pre ktoreho sa zmeni mailova adresa.
     * @param newEmail 
     */
    public void alterEmail(long userId, String newEmail);
    
    /**
     * Zmena krstneho mena.
     * @param userId
     * @param newFirstname 
     */
    public void alterFirstname(long userId, String newFirstname);
    
    /**
     * Zmena priezviska.
     * @param  userId  id pouzivatela, pre ktoreho sa zmeni priezvisko.
     * @param newLastname 
     */
    public void alterLastname(long userId, String newLastname);
    
    /**
     * Zmena hesla.
     * @param userId id pouzivatela, pre ktoreho sa zmeni heslo.
     * @param newPass 
     */
    public void alterPass(long userId, String newPass);
    
    /**
     * Zmena telefonneho cisla.
     * @param userId  id pouzivatela, pre ktoreho sa zmeni telefonne cislo
     * @param newPhone 
     */
    public void alterPhone(long userId, String newPhone);
    
    /**
     * Zmena oblubenych sportov.
     * @param userId
     * @param newSports 
     */
    public void alterSports(long userId, List<SportsE> newSports);
    
    /**
     * Zmena rezervacii platnych pre daneho pouzivatela.
     * @param userId
     * @param newReservations
     */
    public void alterReservations(long userId, List<ReservationE> newReservations);
    
    /**
     * Zmena vsetkych udajov patriacich k pouzivatelovi s id {@code userId} na udaje
     * ziskane z objektu pouzivatela {@code newDataFromUser}.
     * @param userId
     * @param newDataFromUser 
     */
    public void alterUser(long userId, UserE newDataFromUser);
    
    /**
     * Zoznam vsetkych pouzivatelov, ktory si nastavili vo svojom profile typ
     * oblubeneho sportu s identifikatorom sportId.
     * @param sportId
     * @return 
     */
    public List<UserE> getUsersBySportId(long sportId);     
       
    /**
     * Zmena opravnenia pouzivatela.
     * @param userId
     * @param isAdmin true ak je pouzivatel administrator, inak false 
     */
    public void alterAdmin(long userId, boolean isAdmin);
    
    /**
     * Metoda vrati vsetkych administratorov
     */
    public List<UserE> getAllAdmins();
    
}
