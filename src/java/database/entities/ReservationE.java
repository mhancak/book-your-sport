/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import org.primefaces.model.ScheduleEvent;

/**
 *
 * @author gcervena
 */
@Entity
public class ReservationE implements Serializable, ScheduleEvent {
    
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long reservationID;
    @Column(name="REQUIRED_PLAYERS")
    private int reqPlayers;
    @ManyToOne
    private UserE userE;
    @OneToOne
    private SportsE sport;
    @OneToOne
    private SportFacilityE sportFacility;
    @OneToMany(cascade= CascadeType.ALL)
    private List<SubPlayersE> subPlayers;
    
    //Premenne pre DefaultScheduleEvent
    @Column(name="EVENT_ID")
    private String id;
    @Column(name="EVENT_TITLE")
    private String title;
    @Column(name="EVENT_DATE")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name="EVENT_ENDDATE")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name="EVENT_ALLDAY")
    private boolean allDay = false;
    @Column(name="EVENT_STYLECLASS")
    private String styleClass;    
    @Column(name="EVENT_DATA")
    private String data; 
    @Column(name="EVENT_EDITABLE")
    private boolean editable = true;
    
    
    public ReservationE() {
    }
    
    public ReservationE(String title, Date start, Date end){
        this.title = title;
        this.startDate = start;
        this.endDate = end;
    }

    public ReservationE(UserE userE, SportsE sport, SportFacilityE sportFacility,
                        String title, Date start, Date end) {
        this.userE = userE;
        this.sport = sport;
        this.sportFacility = sportFacility;
        this.title = title;
        this.startDate = start;
        this.endDate = end;
    }

    public ReservationE(int reqPlayers, UserE userE, SportsE sport, SportFacilityE sportFacility, List<SubPlayersE> subPlayers,
                        String title, Date start, Date end) {       
        this.reqPlayers = reqPlayers;
        this.userE = userE;
        this.sport = sport;
        this.sportFacility = sportFacility;
        this.subPlayers = subPlayers;
        this.title = title;
        this.startDate = start;
        this.endDate = end;
    }

    public Long getReservationID() {
        return reservationID;
    }

    public void setReservationID(Long reservationID) {
        this.reservationID = reservationID;
    }

    public int getReqPlayers() {
        return reqPlayers;
    }

    public void setReqPlayers(int reqPlayers) {
        this.reqPlayers = reqPlayers;
    }

    public SportsE getSport() {
        return sport;
    }

    public void setSport(SportsE sport) {
        this.sport = sport;
    }

    public SportFacilityE getSportFacility() {
        return sportFacility;
    }

    public void setSportFacility(SportFacilityE sportFacility) {
        this.sportFacility = sportFacility;
    }

    public List<SubPlayersE> getSubPlayers() {
        return subPlayers;
    }

    public void setSubPlayers(List<SubPlayersE> subPlayers) {
        this.subPlayers = subPlayers;
    }

    public UserE getUserE() {
        return userE;
    }

    public void setUserE(UserE userE) {
        this.userE = userE;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reservationID != null ? reservationID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReservationE)) {
            return false;
        }
        ReservationE other = (ReservationE) object;
        if ((this.reservationID == null && other.reservationID != null) || (this.reservationID != null && !this.reservationID.equals(other.reservationID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "database.entities.reservationE[ id=" + reservationID + " ]";
    }

    //Implementacia metod rozhrania ScheduleEvent
    
    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Object getData() {
        return data;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public Date getEndDate() {
        return endDate;
    }

    @Override
    public boolean isAllDay() {
        return allDay;
    }

    @Override
    public String getStyleClass() {
        return styleClass;
    }

    @Override
    public boolean isEditable() {
        return editable;
    }
    
    //Getters and setters

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }
}
