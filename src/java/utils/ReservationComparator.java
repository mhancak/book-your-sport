/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import database.entities.ReservationE;
import java.util.Comparator;
import org.primefaces.model.SortOrder;

/**
 * Komparator pre rezervacie.
 *
 * @author mato
 */
public class ReservationComparator implements Comparator<ReservationE> {

    private String sortField;
    private SortOrder sortOrder;

    public ReservationComparator(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    @Override
    public int compare(ReservationE o1, ReservationE o2) {
        int value = 0;

        if (sortField.equals("title")) {
            value = o1.getTitle().compareTo(o2.getTitle());
        }
        else if (sortField.equals("startDate")) {
            value = o1.getStartDate().compareTo(o2.getStartDate());
        }
        else if (sortField.equals("endDate")) {
            value = o1.getEndDate().compareTo(o2.getEndDate());
        }
        else if (sortField.equals("sportFacility.name")) {
            value = o1.getSportFacility().getName().compareTo(o2.getSportFacility().getName());
        }
        else if (sortField.equals("sport.name")) {
            value = o1.getSport().getName().compareTo(o2.getSport().getName());
        }
        else if (sortField.equals("reserv.userE.email")) {
            value = o1.getUserE().getEmail().compareTo(o2.getUserE().getEmail());
        }

        return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
    }
}
