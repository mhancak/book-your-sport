/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import database.entities.SportsE;
import java.util.Comparator;

/**
 * Komparator pre sporty.
 *
 * @author mato
 */
public class SportComparator implements Comparator<SportsE> {

    @Override
    public int compare(SportsE o1, SportsE o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
