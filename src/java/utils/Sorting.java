/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import database.entities.ReservationE;
import database.entities.SportFacilityE;
import database.entities.SportsE;
import java.util.Collections;
import java.util.List;
import org.primefaces.model.SortOrder;

/**
 * Zbierka metod pre triedenie.
 *
 * @author mato
 */
public class Sorting {
    /** Jeden komparator pre sportoviska v celej aplikacii */
    private static final SportFacilityComparator sportFacilityComparator = new SportFacilityComparator();
    /** Jeden komparator pre sporty v celej aplikacii*/
    private static final SportComparator sportComparator = new SportComparator();

    /**
     * Metoda usporiada sportove zariadenia podla abecedy. Metoda modifikuje povodny list.
     * @param sportFacilities
     * @return 
     */
    public static List<SportFacilityE> sortSportsFacilities(List<SportFacilityE> sportFacilities) {      
        Collections.sort(sportFacilities, sportFacilityComparator);   
        return sportFacilities;
    }

    /**
     * Metoda usporiada sporty podla abecedy. Metoda modifikuje povodny list.
     * @param sports
     * @return 
     */
    public static List<SportsE> sortSports(List<SportsE> sports) {
        Collections.sort(sports, sportComparator);
        return sports;
    }

    /**
     * Metoda usporiada rezervacie bola casu kedy zacinaju. Metoda modifikuje povodny list.
     * @param reservations
     * @return 
     */
    public List<ReservationE> sortReservationsByStartDate(List<ReservationE> reservations) {
        Collections.sort(reservations, new ReservationComparator("startDate", SortOrder.ASCENDING));
        return reservations;
    }    
}
