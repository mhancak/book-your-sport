/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import database.entities.SportFacilityE;
import java.util.Comparator;

/**
 * Komparator pre sportove zariadenia.
 *
 * @author mato
 */
public class SportFacilityComparator implements Comparator<SportFacilityE> {

    @Override
    public int compare(SportFacilityE m1, SportFacilityE m2) {
        return m1.getName().compareTo(m2.getName());
    }
}
