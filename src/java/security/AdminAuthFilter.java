/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package security;

import java.io.IOException;
import javax.faces.context.FacesContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import managedBeans.LoginBean;

/**
 * Trieda filtruje poziadavky na adresu administratorskeho rozhrania. Ak
 * uzivatel nie je administrator tak sa k admin stranke nedostane.
 *
 * @author mato
 */
public class AdminAuthFilter implements Filter {

    private FilterConfig config;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.config = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        LoginBean lb = (LoginBean) ((HttpServletRequest) request).getSession().getAttribute("loginBean");
        
        if (!lb.isAdminLoggin()) {
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_NOT_FOUND);          
        }
        else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        config = null;
    }
}
