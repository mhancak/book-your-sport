/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package security;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Gabo
 */
public class PasswordSecurity {
    
    /**
     * Konštruktor triedy PasswordSecurity
     */
    public PasswordSecurity(){
    }
    
        /**
     * Sifrovanie hesla
     * 
     * @param password
     * @return zasifrovany retazec (heslo)
     */
    public String encryption(String password) {
        if (password == null) return null;

        SecureRandom random;
        byte[] bSalt;

        try {
            random = SecureRandom.getInstance("SHA1PRNG");
            bSalt = new byte[8];
            random.nextBytes(bSalt);
            password = hash(password, bSalt); // test test test
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(PasswordSecurity.class.getName()).log(Level.SEVERE, null, ex);
        }
        return password;
    }
    
        /**
     * Porovnava zadane a v databaze ulozene heslo
     * 
     * @param dbPassword heslo ulozene v databaze (zasifrovane)
     * @param userPassword heslo zadane pouzivatelom
     * @return boolean, ci sa zadane heslo zhoduje s heslom ulozenym v databaze pre daneho pouzivatela
     */
    public boolean decryption(String dbPassword, String userPassword) {
        if (dbPassword == null || userPassword == null) return false;

        try {
            String salt = dbPassword.substring(dbPassword.length() - 12, dbPassword.length());
            byte[] bSalt;
            bSalt = base64ToByte(salt);
            userPassword = hash(userPassword, bSalt);
        } catch (IOException ex) {
            Logger.getLogger(PasswordSecurity.class.getName()).log(Level.SEVERE, null, ex);
        }

        return userPassword.equals(dbPassword);
    }
    
        /**
     * Pomocou salt-u a hash-u zasifruje heslo
     * 
     * @param password heslo, ktore chceme zasifrovat
     * @param bSalt salt
     * @return zasifrovane heslo
     */
    public String hash(String password, byte[] bSalt) {
        if (password == null || bSalt == null) return null;
        
        byte[] input;
        try {
            password = password.concat(byteToBase64(bSalt));
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.reset();
            digest.update(password.getBytes());
            input = digest.digest(password.getBytes("UTF-8"));
            password = byteToBase64(input);
            password = password.toString().concat(byteToBase64(bSalt));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(PasswordSecurity.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex){
            Logger.getLogger(PasswordSecurity.class.getName()).log(Level.SEVERE, null, ex);
        }
        return password;
    }
    
        /**
     * Z base 64 reprezentacie urobi korespondujucu bitovu reprezentaciu
     *
     * @param data String base64 reprezentacie
     * @return byte[]
     * @throws IOException
     */
    public static byte[] base64ToByte(String data) throws IOException {
        BASE64Decoder decoder = new BASE64Decoder();
        return decoder.decodeBuffer(data);
    }

    /**
     * Z bitovej reprezentacie vrati base 64 reprezentaciu
     *
     * @param data pole bajtov
     * @return String
     */
    public static String byteToBase64(byte[] data) {
        BASE64Encoder endecoder = new BASE64Encoder();
        return endecoder.encode(data);
    }
}
