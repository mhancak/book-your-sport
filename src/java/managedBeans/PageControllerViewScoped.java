/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import com.ocpsoft.pretty.PrettyContext;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Gaboxxxxx
 */
@ManagedBean
@ViewScoped
public class PageControllerViewScoped implements Serializable {

    private boolean reservationButton = false;

    @PostConstruct
    public void init() {
        if (PrettyContext.getCurrentInstance().getCurrentMapping() != null) {
            String page = PrettyContext.getCurrentInstance().getCurrentMapping().getId();
            if (page.equals("table")) {
                reservationButton = true;
            }
        }
    }
    
    public boolean isReservationButton() {
        return reservationButton;
    }

    public void setReservationButton(boolean reservationButton) {
        this.reservationButton = reservationButton;
    }
}
