/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.vbnvbn
 */
package managedBeans;

import database.entities.UserE;
import database.service.UserService;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 * Manazovanie nastaveni pouzivatela na stranke nastavenia pouzivatela.
 * @author mato
 */
@ManagedBean
@ViewScoped
public class UserSettingsBean implements Serializable{
    /** 
     * Duplikat prihlaseneho pouzivatela pre zmenu osobnych udajov. 
     * Pomocou userID sa tento uzivatel zluci s povodnym pouzivatelom.
     */
    private UserE updatedUser;
    /** Manazovany bean pre spravu prihlaseneho pouzivatela */    
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    /** Manazovany bean pre ziskanie lokalizacie */
    @ManagedProperty(value = "#{language}")
    private LanguageBean languageBean;
    /** Manazovany bean pre pristup k tabulke pouzivatela v databaze */
    @ManagedProperty(value = "#{userService}")
    private UserService userService;
    
    /** 
     * Aktualne heslo. Heslo, ktore prave pouziva pouzivatel. Potrebne na
     * potvrdenie zmien nastaveni.
     */
    private String currentPassword;
    /** Potvrdenie NOVEHO heslo */
    private String confirmPassword;

    /**
     * Update uzivatela v databaze na nove udaje.
     */
    public void updateUser() {
        FacesContext cont = FacesContext.getCurrentInstance();
        ResourceBundle resBundle = ResourceBundle.
                getBundle("localization/web/registration", languageBean.getLocale());
        ResourceBundle resBundleU = ResourceBundle.
                getBundle("localization/web/usersettings", getLanguageBean().getLocale());

        FacesMessage msg;

        boolean existMail = userService.existsEmail(updatedUser.getEmail());
        //ak si pouzivatel menil email na taky, ktory existuje uz v databaze
        if (!updatedUser.getEmail().equals(loginBean.getUser().getEmail()) && existMail) {
            String rawMessage = resBundle.getString("email.userexist");
            String messageTxt = MessageFormat.format(rawMessage, updatedUser.getEmail());

            msg = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, messageTxt, "");
            
            cont.addMessage(null, msg);            
        } 
        //ak mail neexistuje v databaze alebo pouzivatel nemenil mail
        else if (!existMail || updatedUser.getEmail().equals(loginBean.getUser().getEmail())) {
            //ak sa nove heslo a povodne nerovnaju, tj dojde ku zmene hesla
            if (!updatedUser.getPasswd().equals(loginBean.getPassword())) {
                //a zaroven chyba potvrdenie hesla
                if (confirmPassword == null || confirmPassword.equals("")) {
                    msg = new FacesMessage(
                            FacesMessage.SEVERITY_ERROR, resBundle.getString("password.confirmmessage"), "");
                    cont.addMessage(null, msg);
                    return;
                } 
                //alebo potvrdene heslo je ine ako napisane
                else if (!confirmPassword.equals(updatedUser.getPasswd())) {
                    msg = new FacesMessage(
                            FacesMessage.SEVERITY_ERROR, resBundle.getString("password.confirmmessagenoteq"), "");
                    cont.addMessage(null, msg);
                    return;
                }
                //inak je validacia noveho hesla OK                
            }
                        
            //ak sedi aktualne heslo s heslom v DB a su overene vsetky poziadavky vykonaj transakciu
            if (currentPassword.equals(loginBean.getPassword())){
                //samotna aktualizacia uzivatela v databaze
                getUserService().alterUser(loginBean.getUser().getId(), updatedUser);
                setDataToUser(updatedUser, loginBean.getUser());

                msg = new FacesMessage(
                    FacesMessage.SEVERITY_INFO, resBundleU.getString("updateuser.success"), "");
                cont.addMessage(null, msg);
            }
            //ak nesedi aktualne heslo s heslom v DB
            else{
                msg = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, resBundleU.getString("password.badcurrentpassword"), "");
                cont.addMessage(null, msg);                
            }
        }
    }
    
    /**
     * Aktualizacia oblubenych sportov prihlaseneho pouzivatela.
     */
    public void updateUserSports() {
        getUserService().alterSports(loginBean.getUser().getId(), updatedUser.getSports());
        loginBean.getUser().setSports(updatedUser.getSports());
        
        FacesContext cont = FacesContext.getCurrentInstance();
        ResourceBundle resBundleU = ResourceBundle.
                getBundle("localization/web/usersettings", getLanguageBean().getLocale());
        FacesMessage msg = new FacesMessage(
                    FacesMessage.SEVERITY_INFO, resBundleU.getString("updateusersports.success"), "");
        cont.addMessage(null, msg);
    }

    /**
     * @return the loginBean
     */
    public LoginBean getLoginBean() {
        return loginBean;
    }

    /**
     * @param loginBean the loginBean to set
     */
    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
        this.updatedUser = createNewUserCopy(loginBean.getUser());
    }

    /**
     * Vytvorenie "deep" kopie pouzivatela. Pouzivatelove ID sa nekopiruje.
     * @param originUser
     * @return 
     */
    private UserE createNewUserCopy(UserE originUser) {
        UserE user = new UserE(originUser.getEmail(),
                originUser.getFirsname(),
                originUser.getLastname(),
                originUser.getPasswd(),
                originUser.getPhone(),
                originUser.getSports(),
                originUser.getReservations());
        user.setAdmin(originUser.isAdmin());  
        return user;
    }
    
    /**
     * Aktulizacia potrebnych (nie vsetkych) udajov medzi uzivatelmi. 
     * @param from
     * @param to 
     */
    private void setDataToUser(UserE from, UserE to){
        to.setEmail(from.getEmail());
        to.setFirsname(from.getFirsname());
        to.setLastname(from.getLastname());
        to.setPasswd(from.getPasswd());
        to.setPhone(from.getPhone());
    }

    /**
     * @return the languageBean
     */
    public LanguageBean getLanguageBean() {
        return languageBean;
    }

    /**
     * @param languageBean the languageBean to set
     */
    public void setLanguageBean(LanguageBean languageBean) {
        this.languageBean = languageBean;
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the updatedUser
     */
    public UserE getUpdatedUser() {
        return updatedUser;
    }

    /**
     * @param updatedUser the updatedUser to set
     */
    public void setUpdatedUser(UserE updatedUser) {
        this.updatedUser = updatedUser;
    }

    /**
     * @return the currentPassword
     */
    public String getCurrentPassword() {
        return currentPassword;
    }

    /**
     * @param currentPassword the currentPassword to set
     */
    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    /**
     * @return the confirmPassword
     */
    public String getConfirmPassword() {
        return confirmPassword;
    }

    /**
     * @param confirmPassword the confirmPassword to set
     */
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
