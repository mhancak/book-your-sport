/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;



import java.util.ResourceBundle;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.mail.MessagingException;

/**
 *
 * @author Lukas
 */
@ManagedBean
@SessionScoped
public class ClientBean {
    @ManagedProperty(value="#{language}")
    private LanguageBean languageBean;
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStatusMessage() {
        return statusMessage;
    }
    
    /**
     * @return the languageBean
     */
    public LanguageBean getLanguageBean() {
        return languageBean;
    }

    /**
     * @param languageBean the languageBean to set
     */
    public void setLanguageBean(LanguageBean languageBean) {
        this.languageBean = languageBean;
    }
    
    public String send() {
        ResourceBundle resBundle = ResourceBundle.getBundle
                    ("localization/web/contactus", languageBean.getLocale());
        System.out.println(resBundle.getString("output"));
        statusMessage = resBundle.getString("output");//"Message Sent";
        try {
            MailService.sendMessage(recipient, subject, message);
        }
        catch(MessagingException ex) {
            statusMessage = ex.getMessage();
        }
        return "contactus?faces-redirect=true";  // redisplay page with status message
    }
    
    private String recipient;
    private String subject;
    private String message;
    private String statusMessage = "";
}
