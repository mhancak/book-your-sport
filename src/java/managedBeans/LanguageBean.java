/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.cvbcvb
 */
package managedBeans;

import java.io.Serializable;
import java.util.Locale;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

/**
 * Bean pre spravu lokalizacie.
 * @author mato
 */
@ManagedBean(name="language")
@SessionScoped
public class LanguageBean implements Serializable{
    
    private Locale locale;
    private boolean insertJS;  
    
    public LanguageBean(){
        Locale tl = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        if (tl.getCountry() == null || tl.getCountry().equals("")){
            this.locale = new Locale(tl.getLanguage(), tl.getLanguage().toUpperCase());
        }
        else{
            this.locale = new Locale(tl.getLanguage(), tl.getCountry());
        }
                
        setInsertJS();
    }
    
    public Locale getLocale() {
        return locale;
    }

    public String getLanguage() {
        return locale.getLanguage();
    }   
    
    private void setInsertJS(){
        if (this.locale.getLanguage().toLowerCase().contains("en")){
            insertJS = false;
        }
        else{
            insertJS = true;
        }
    }
    
    public void changeLanguage(ActionEvent event) {  
        String id = event.getComponent().getId();  
        String[] localeArray = id.split("_");        
        this.locale = new Locale(localeArray[0], localeArray[1]);
        
        setInsertJS();
        
        FacesContext.getCurrentInstance().getViewRoot().setLocale(this.locale); 
    }

    /**
     * @return the insertJS
     */
    public boolean isInsertJS() {
        return this.insertJS;
    }
    
}
