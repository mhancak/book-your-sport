/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import database.entities.UserE;
import database.service.ReservationService;
import database.service.SportsService;
import database.service.UserService;
import java.text.MessageFormat;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.mail.MessagingException;

/**
 *
 * @author Lukas
 */
@ManagedBean(name = "emailNotice")
@SessionScoped
public class EmailNoticeBean {

    private List<String> mail;
    private String sport;
    private String date;
    private String recipient;
    private String subject;
    private String message;
    private String statusMessage = "";
    @ManagedProperty(value = "#{language}")
    private LanguageBean languageBean;

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    /**
     * @return the languageBean
     */
    public LanguageBean getLanguageBean() {
        return languageBean;
    }

    /**
     * @param languageBean the languageBean to set
     */
    public void setLanguageBean(LanguageBean languageBean) {
        this.languageBean = languageBean;
    }

    public void send() {
        ResourceBundle resBundle = ResourceBundle.getBundle("localization/web/emailnotice", languageBean.getLocale());
        String messageR = resBundle.getString("notice");
        String messageTxt = MessageFormat.format(messageR, this.getSport(), this.getDate());
        this.message = messageTxt;//"welcome text";
        this.subject = resBundle.getString("subject");//subject of the mail
        try {
            for (String email : getMail()) {
                recipient = email;
                MailService.sendMessage(recipient, subject, message);
            }
        } catch (MessagingException ex) {
            statusMessage = ex.getMessage();
        }
    }

    /**
     * @return the mail
     */
    public List<String> getMail() {
        return mail;
    }

    /**
     * @param mail the mail to set
     */
    public void setMail(List<String> mail) {
        this.mail = mail;
    }

    /**
     * @return the sport
     */
    public String getSport() {
        return sport;
    }

    /**
     * @param sport the sport to set
     */
    public void setSport(String sport) {
        this.sport = sport;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }
}
