/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import java.util.ArrayList;  
import java.util.List; 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
/**
 *
 * @author Lukas
 */
@ManagedBean(name="imageSwitch")
@SessionScoped
public class ImageSwitchBean {  
  
    private List<String> images;  
  
    public ImageSwitchBean() {  
        images = new ArrayList<String>();  
        images.add("athlete.png");    
        images.add("handball.png");
        images.add("runner.png");
        images.add("running.png");
        images.add("tennis_player.png");
    }  
  
    public List<String> getImages() {  
        return images;  
    }  
} 
