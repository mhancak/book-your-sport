/*
 * To change this template, choose Tools | Templates
 * and open the template in the ecvbcvbditor.
 */
package managedBeans;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.mail.MessagingException;

/**
 *
 * @author Lukas
 */
@ManagedBean(name="passwdForger")
@SessionScoped
public class PasswdForgetBean {
    
    private String password;    
    private String recipient;
    private String subject;
    private String message;
    private String statusMessage = "";
    @ManagedProperty(value="#{language}")
    private LanguageBean languageBean;

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }
    
    /**
     * @return the languageBean
     */
    public LanguageBean getLanguageBean() {
        return languageBean;
    }

    /**
     * @param languageBean the languageBean to set
     */
    public void setLanguageBean(LanguageBean languageBean) {
        this.languageBean = languageBean;
    }
    
    public void send() {
        ResourceBundle resBundle = ResourceBundle.getBundle
                    ("localization/web/login", languageBean.getLocale());
        String messageR = resBundle.getString("message"); 
        String messageTxt = MessageFormat.format(messageR, this.getPassword());
        this.message = messageTxt;//"welcome text";
        this.subject = resBundle.getString("subject");//subject of the mail
        try {
            MailService.sendMessage(recipient, subject, message);
        }
        catch(MessagingException ex) {
            statusMessage = ex.getMessage();
        }
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
