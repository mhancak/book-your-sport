/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.xc xcvxcv
 */
package managedBeans;

import database.entities.UserE;
import database.service.UserService;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Lukas
 */
@ManagedBean
@ViewScoped
public class UserManagedBean {

    private UserE user = new UserE();
    @ManagedProperty(value="#{userService}")
    private UserService userService;
    private String passwordConfirm;
    @ManagedProperty(value="#{language}")
    private LanguageBean languageBean;
    @ManagedProperty(value="#{welcome}")
    private WelcomeEmailBean welcome;

    public String addUser() {        
        if (validateData()){
            getUserService().createUser(user);
            getWelcome().setRecipient(user.getEmail());
            getWelcome().setMail(user.getEmail());
            getWelcome().send();
            return "home?faces-redirect=true";
        }
        else{
            return null;
        }        
    }
        
    /**
     * Validacia ci existuje email v databaze ci druhe heslo sa zhoduje
     * s prvym. Zaroven sa vytvoria potrebne hlasky
     * @return 
     */
    public boolean validateData(){
        FacesContext cont = FacesContext.getCurrentInstance();      
        ResourceBundle resBundle = ResourceBundle.
                getBundle("localization/web/registration", languageBean.getLocale());
        
        if (userService.existsEmail(user.getEmail())){
            String rawMessage = resBundle.getString("email.userexist"); 
            String messageTxt = MessageFormat.format(rawMessage, user.getEmail());
            
            FacesMessage msg = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, messageTxt, ""); 
            cont.addMessage(null, msg);       
            return false;
        }
        else if (!user.getPasswd().equals(this.passwordConfirm)){
            FacesMessage msg = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, resBundle.getString("password.confirmmessagenoteq"), ""); 
            cont.addMessage(null, msg);       
            return false;
        }
        return true;
    }

    /**
     * Resets the user data back to the initial values.
     *
     */
    public void reset() {
        setUser(new UserE());
    }

    public UserE getUser() {
        return user;
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the passwordConfirm
     */
    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    /**
     * @param passwordConfirm the passwordConfirm to set
     */
    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    /**
     * @return the languageBean
     */
    public LanguageBean getLanguageBean() {
        return languageBean;
    }

    /**
     * @param languageBean the languageBean to set
     */
    public void setLanguageBean(LanguageBean languageBean) {
        this.languageBean = languageBean;
    }

    /**
     * @param user the user to set
     */
    public void setUser(UserE user) {
        this.user = user;
    }

    /**
     * @return the welcome
     */
    public WelcomeEmailBean getWelcome() {
        return welcome;
    }

    /**
     * @param welcome the welcome to set
     */
    public void setWelcome(WelcomeEmailBean welcome) {
        this.welcome = welcome;
    }
}
