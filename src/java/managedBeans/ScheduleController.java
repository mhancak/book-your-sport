/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import database.entities.ReservationE;
import database.entities.SportFacilityE;
import database.entities.SportsE;
import database.entities.SubPlayersE;
import database.entities.UserE;
import database.service.ReservationService;
import database.service.SportsService;
import database.service.SubPlayersService;
import database.service.UserService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.context.RequestContext;
import org.primefaces.event.*;
import org.primefaces.model.*;

/**
 *
 * @author Gabo
 */
@ManagedBean(name = "scheduleController")
@SessionScoped
public class ScheduleController {

    private ScheduleModel eventModel;
    private ScheduleEvent event = new ReservationE();
    private String theme;
    private String selectedDate;
    private String selectedTime;
    private String selectedEndTime;
    private String selectedDuration;
    private boolean renderSportsList;
    private int subPlayersNeeded;
    private int subPlayersNeededDTO;
    private int subPlayersToAdd;
    //Premenne pre nacitanie sportov do selecOneMenu
    private List<SportsE> sports;
    private SportsE selectedSport;
    @ManagedProperty(value = "#{sportsService}")
    private SportsService sportsService;
    //pre posielanie mailov pri subplayers
    private List<UserE> users;
    private List<String> emails;
    //Managed Properties
    @ManagedProperty(value = "#{reservationService}")
    private ReservationService reservationService;
    @ManagedProperty(value = "#{loginBean}")
    private LoginBean loginBean;
    @ManagedProperty(value = "#{sportFacilityBean}")
    private SportFacilityBean sportFacilityBean;
    @ManagedProperty(value = "#{subPlayersService}")
    private SubPlayersService subPlayersService;
    @ManagedProperty(value = "#{userService}")
    private UserService userService;
    @ManagedProperty(value = "#{emailNotice}")
    private EmailNoticeBean emailNotice;

    public ScheduleController() {
        eventModel = new DefaultScheduleModel();
    }

    @PostConstruct
    private void init() {
        SportFacilityE sportFacility = sportFacilityBean.getSelectedSportFacility();
        eventModel.clear();
        List<ReservationE> reservations;
        reservations = reservationService.getReservationsBySportFacilityId(sportFacility.getId());
        for (ReservationE reservation : reservations) {
            eventModel.addEvent(reservation);
        }
    }

    public void sendNoticeEmails() {
        emails = new ArrayList<String>();
        if (subPlayersNeeded > 0) {
            users = userService.getUsersBySportId(selectedSport.getId());
            for (UserE user : users) {
                emails.add(user.getEmail());
            }
            emailNotice.setMail(emails);
            emailNotice.setSport(selectedSport.getName());
            emailNotice.setDate(selectedDate);
            emailNotice.send();
        }
    }

    public void sendNoticeEmailsUpdate() {
        emails = new ArrayList<String>();
        ReservationE reserv = reservationService.getReservationById(((ReservationE) event).getReservationID());
        if (reserv.getReqPlayers() > 0) {
            users = userService.getUsersBySportId(reserv.getSport().getId());
            for (UserE user : users) {
                emails.add(user.getEmail());
            }
            emailNotice.setMail(emails);
            emailNotice.setSport(selectedSport.getName());
            emailNotice.setDate(selectedDate);
            emailNotice.send();
        }
    }

    public void addEvent(ActionEvent actionEvent) {
        Date startDate = event.getStartDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        if (selectedDuration.equals("default") || (selectedSport.getId() == null)) {
            if (selectedDuration.equals("default") && (selectedSport.getId() == null)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Input error", "Select time !\nSelect sport !"));
            } else if (selectedSport.getId() == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Input error", "Select sport !"));
            } else if (selectedDuration.equals("default")) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Input error", "Select time !"));
            }
        } else {
            switch (Integer.parseInt(selectedDuration)) {
                case 1:
                    calendar.add(Calendar.HOUR_OF_DAY, 1);
                    break;
                case 2:
                    calendar.add(Calendar.HOUR_OF_DAY, 1);
                    calendar.add(Calendar.MINUTE, 30);
                    break;
                case 3:
                    calendar.add(Calendar.HOUR_OF_DAY, 2);
                    break;
                default:
                    break;
            }

            ((ReservationE) event).setEndDate(calendar.getTime());
            ((ReservationE) event).setSport(selectedSport);
            ((ReservationE) event).setTitle(event.getTitle() + "\n" + selectedSport.getName());
            ((ReservationE) event).setUserE(loginBean.getUser());
            ((ReservationE) event).setSportFacility(sportFacilityBean.getSelectedSportFacility());
            ((ReservationE) event).setReqPlayers(subPlayersNeeded);
            if (subPlayersNeeded > 0) {
                ((ReservationE) event).setStyleClass("greenEvent");
            }

            if (eventCanBeStored(event)) {
                if (event.getId() == null) {
                    eventModel.addEvent(event);
                } else {
                    eventModel.updateEvent(event);
                }

                System.out.println("----------------------------------Storing into database----------------------------------");
                reservationService.createReservation((ReservationE) event);
                RequestContext.getCurrentInstance().execute("myschedule.update();eventDialog.hide();");
                this.sendNoticeEmails();
                //Reset premennych
                event = new ReservationE();
                selectedDuration = null;
                selectedSport = null;
                subPlayersNeeded = 0;
            }
        }
    }

    public void updateEvent() {
        if (subPlayersToAdd != 0) {
            List<SubPlayersE> subPlayers;
            if (((ReservationE) event).getSubPlayers().isEmpty()) {
                subPlayers = new ArrayList<SubPlayersE>();
            } else {
                subPlayers = ((ReservationE) event).getSubPlayers();
            }

            SubPlayersE subPlayersEntity = new SubPlayersE();
            subPlayersEntity.setCount(subPlayersToAdd);
            subPlayersEntity.setUser(loginBean.getUser());
            subPlayers.add(subPlayersEntity);

            subPlayersNeeded = subPlayersNeededDTO - subPlayersToAdd;
            if(subPlayersNeeded == 0){
                ((ReservationE) event).setStyleClass(null);
            }
            ((ReservationE) event).setReqPlayers(subPlayersNeeded);
            ((ReservationE) event).setSubPlayers(subPlayers);
            reservationService.deleteReservation(((ReservationE) event).getReservationID());
            reservationService.createReservation((ReservationE) event);
            eventModel.updateEvent(((ReservationE) event));
            RequestContext.getCurrentInstance().execute("myschedule.update();eventEditDialog.hide();");
        }
    }

    public void editEvent() {
        if (selectedSport.getId() != null) {
            ((ReservationE) event).setSport(selectedSport);
        } else{
            selectedSport = ((ReservationE) event).getSport();
        }
        if (subPlayersNeeded != 0) {
            ((ReservationE) event).setStyleClass("greenEvent");
        } else{
            ((ReservationE) event).setStyleClass(null);
        }
        ((ReservationE) event).setReqPlayers(subPlayersNeeded);
        ((ReservationE) event).setTitle(loginBean.getUser().getFirsname().substring(0, 1) + "." + loginBean.getUser().getLastname() + "\n" + selectedSport.getName());
        reservationService.deleteReservation(((ReservationE) event).getReservationID());
        reservationService.createReservation((ReservationE) event);
        this.sendNoticeEmailsUpdate();
        eventModel.updateEvent(((ReservationE) event));
        RequestContext.getCurrentInstance().execute("myschedule.update();eventEditDialog.hide();");
    }

    public void deleteEvent() {
        reservationService.deleteReservation(((ReservationE) event).getReservationID());
        eventModel.deleteEvent(event);
        RequestContext.getCurrentInstance().execute("myschedule.update();eventEditDialog.hide();");
    }

    public void onEventSelect(ScheduleEntrySelectEvent selectEvent) {
        SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat time = new SimpleDateFormat("HH:mm");
        event = findReservationByTime(selectEvent.getScheduleEvent().getStartDate(), selectEvent.getScheduleEvent().getEndDate());
        selectedDate = date.format(event.getStartDate());
        selectedTime = time.format(event.getStartDate());
        selectedEndTime = time.format(event.getEndDate());
        subPlayersNeeded = ((ReservationE) event).getReqPlayers();
        subPlayersNeededDTO = subPlayersNeeded;
        //((ReservationE)event).getUserE().equals(loginBean.getUser());
    }

    public void onDateSelect(DateSelectEvent selectEvent) {
        selectedDuration = null;
        selectedSport = null;
        SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat time = new SimpleDateFormat("HH:mm");
        selectedDate = date.format(selectEvent.getDate());
        selectedTime = time.format(selectEvent.getDate());
        event = new ReservationE(loginBean.getUser().getFirsname().substring(0, 1) + "." + loginBean.getUser().getLastname(), selectEvent.getDate(), selectEvent.getDate());
    }

    public void onEventMove(ScheduleEntryMoveEvent moveEvent) {

        event = moveEvent.getScheduleEvent();
        Date newStartDate = this.event.getStartDate();
        Date newEndDate = this.event.getEndDate();

        if (loginBean.isLoggedIn()) {

            if (loginBean.getUser().equals(((ReservationE) event).getUserE())) {

                if (eventCanBeStored(moveEvent.getScheduleEvent())) {

                    System.out.println("----- NEW DATES ----- " + newStartDate + " ----- " + newEndDate);

                    ReservationE reservationToMove = findReservationByTime(newStartDate, newEndDate);
                    System.out.println("----- RESERVATION ID ------" + reservationToMove.getReservationID());

                    reservationService.deleteReservation(reservationToMove.getReservationID());
                    reservationService.createReservation(reservationToMove);

                    eventModel.updateEvent(reservationToMove);

                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event moved", "Day delta:" + moveEvent.getDayDelta() + ", Minute delta:" + moveEvent.getMinuteDelta());
                    addMessage(message);
                } else {
                    updateSchedule(newStartDate, newEndDate, moveEvent);
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cant move event", "Here is another event!");
                    addMessage(message);
                    RequestContext.getCurrentInstance().execute("myschedule.update();");
                }
            } else {
                updateSchedule(newStartDate, newEndDate, moveEvent);
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Permission error", "You dont have permissions to move this event. You can move only your own events!");
                addMessage(message);
                RequestContext.getCurrentInstance().execute("myschedule.update();");
            }
        } else {
            updateSchedule(newStartDate, newEndDate, moveEvent);
            RequestContext.getCurrentInstance().execute("myschedule.update();errorDialog.show();");
        }
        event = new ReservationE();
    }

    public void onEventResize(ScheduleEntryResizeEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Event resized", "Day delta:" + event.getDayDelta() + ", Minute delta:" + event.getMinuteDelta());

        addMessage(message);
    }

    private void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Metoda usporiada zoznam sportov podla abecedy
     *
     * @param sports zoznam sportov, ktore chceme usporiadat
     * @return zoznam usporiadanych sportov
     */
    private List<SportsE> SortSports(List<SportsE> sports) {
        List<String> sportNames = new ArrayList<String>();
        List<SportsE> sortedSports = new ArrayList<SportsE>();
        for (SportsE sport : sports) {
            sportNames.add(sport.getName());
        }
        Collections.sort(sportNames);
        for (String sportName : sportNames) {
            for (SportsE sport : sports) {
                if (sportName.equals(sport.getName())) {
                    sortedSports.add(sport);
                }
            }
        }
        return sortedSports;
    }

    /**
     * Nacitanie vsetkych rezervacii ulozenych v databaze pre dane sportovisko a
     * reload tabulky
     */
    public void loadReservations() {
        SportFacilityE sportFacility = sportFacilityBean.getSelectedSportFacility();
        eventModel = new DefaultScheduleModel();
        //eventModel.clear();
        List<ReservationE> reservations;
        reservations = reservationService.getReservationsBySportFacilityId(sportFacility.getId());
        for (ReservationE reservation : reservations) {
            eventModel.addEvent(reservation);
        }
        navigate();
    }

    /**
     * Navigacia na stranku table.xhtml (refreshnutie stranky pri zmene
     * sportoviska)
     */
    public void navigate() {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationHandler navigationHandler = context.getApplication().getNavigationHandler();
        navigationHandler.handleNavigation(context, null, "table" + "?faces-redirect=true");
    }

    /**
     * Metoda overuje, ci na dane miesto moze byt ulozena nova rezervacia
     *
     * @param event rezervacia, ktoru chceme ulozit
     * @return boolean, ci je alebo nie je mozne ulozenie
     */
    private boolean eventCanBeStored(ScheduleEvent event) {
        boolean returnValue = true;
        List<ScheduleEvent> reservations = eventModel.getEvents();
        if (event.getStartDate().before(new Date())) {
            returnValue = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Input Error", "Cant store event in the past!"));
        } else {
            for (ScheduleEvent scheduleEvent : reservations) {
                if ((event.getStartDate().after(scheduleEvent.getStartDate())) && (event.getStartDate().before(scheduleEvent.getEndDate()))) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Input error", "In this time is another reservation !"));
                    returnValue = false;
                } else if ((event.getEndDate().after(scheduleEvent.getStartDate())) && (event.getEndDate().before(scheduleEvent.getEndDate()))) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Input error", "In this time is another reservation !"));
                    returnValue = false;
                } else if ((event.getStartDate().before(scheduleEvent.getStartDate())) && (event.getEndDate().after(scheduleEvent.getEndDate()))) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Input error", "In this time is another reservation !"));
                    returnValue = false;
                }
            }
        }
        return returnValue;
    }

    private ReservationE findReservationByTime(Date startDate, Date endDate) {
        ReservationE reservation;
        List<ScheduleEvent> reservations = eventModel.getEvents();
        for (ScheduleEvent scheduleEvent : reservations) {
            if (scheduleEvent.getStartDate().equals(startDate) && scheduleEvent.getEndDate().equals(endDate)) {
                reservation = (ReservationE) scheduleEvent;
                return reservation;
            }
        }
        return null;
    }

    private void updateSchedule(Date newStartDate, Date newEndDate, ScheduleEntryMoveEvent moveEvent) {
        Calendar calendar = Calendar.getInstance();
        Date oldStartDate;
        Date oldEndDate;

        //Get reservation start time before move
        calendar.setTime(newStartDate);
        calendar.add(Calendar.DAY_OF_YEAR, -moveEvent.getDayDelta());
        calendar.add(Calendar.MINUTE, -moveEvent.getMinuteDelta());
        oldStartDate = calendar.getTime();

        //Get reservation end time before move
        calendar.setTime(newEndDate);
        calendar.add(Calendar.DAY_OF_YEAR, -moveEvent.getDayDelta());
        calendar.add(Calendar.MINUTE, -moveEvent.getMinuteDelta());
        oldEndDate = calendar.getTime();

        ReservationE reservationToMove = findReservationByTime(newStartDate, newEndDate);
        reservationToMove.setStartDate(oldStartDate);
        reservationToMove.setEndDate(oldEndDate);

        reservationService.deleteReservation(reservationToMove.getReservationID());
        reservationService.createReservation(reservationToMove);

        eventModel.updateEvent(reservationToMove);
    }

    public boolean isAccordionPanelRendered() {
        if (!loginBean.isLoggedIn()) {
            return false;
        }
        if (event == null) {
            return false;
        }
        if (((ReservationE) event).getSubPlayers() == null) {
            return false;
        }
        if (((ReservationE) event).getSubPlayers().isEmpty()) {
            return false;
        } else {
            List<SubPlayersE> subPlayers = ((ReservationE) event).getSubPlayers();
            for (SubPlayersE subPlayer : subPlayers) {
                if (subPlayer.getUser().equals(loginBean.getUser())) {
                    return true;
                }
            }
        }
        return false;
    }

    public void unjoinSubPlayer() {
        int playersUnjoined = 0;
        int subPlayersRequired = 0;
        List<SubPlayersE> subPlayers = ((ReservationE) event).getSubPlayers();

        for (Iterator<SubPlayersE> iterator = subPlayers.iterator(); iterator.hasNext();) {
            SubPlayersE subPlayer = iterator.next();
            if (subPlayer.getUser().equals(loginBean.getUser())) {
                playersUnjoined = playersUnjoined + subPlayer.getCount();
                iterator.remove();
            }
        }

        subPlayersRequired = ((ReservationE) event).getReqPlayers();
        subPlayersRequired = subPlayersRequired + playersUnjoined;
        if(subPlayersRequired > 0){
            ((ReservationE) event).setStyleClass("greenEvent");
        }
        ((ReservationE) event).setSubPlayers(subPlayers);
        ((ReservationE) event).setReqPlayers(subPlayersRequired);
        reservationService.deleteReservation(((ReservationE) event).getReservationID());
        reservationService.createReservation((ReservationE) event);
        eventModel.updateEvent(((ReservationE) event));
        System.out.println("-------------UNJOINING------------");
        RequestContext.getCurrentInstance().execute("myschedule.update();eventEditDialog.hide();");
    }

    //Gettery a Settery---------------------------------------------------------
    public ScheduleModel getEventModel() {
        return eventModel;
    }

    public void setEventModel(ScheduleModel eventModel) {
        this.eventModel = eventModel;
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(String selectedDate) {
        this.selectedDate = selectedDate;
    }

    public String getSelectedTime() {
        return selectedTime;
    }

    public void setSelectedTime(String selectedTime) {
        this.selectedTime = selectedTime;
    }

    public List<SportsE> getSports() {
        renderSportsList = true;
        sports = sportFacilityBean.getSelectedSportFacility().getSports();
        if (sports.size() == 1) {
            renderSportsList = false;
            selectedSport = sports.get(0);
        }
        return sports;
    }

    public void setSports(List<SportsE> sports) {
        this.sports = sports;
    }

    public SportsE getSelectedSport() {
        return selectedSport;
    }

    public void setSelectedSport(SportsE selectedSport) {
        this.selectedSport = selectedSport;
    }

    public SportsService getSportsService() {
        return sportsService;
    }

    public void setSportsService(SportsService sportsService) {
        this.sportsService = sportsService;
    }

    public ReservationService getReservationService() {
        return reservationService;
    }

    public void setReservationService(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    public LoginBean getLoginBean() {
        return loginBean;
    }

    public void setLoginBean(LoginBean loginBean) {
        this.loginBean = loginBean;
    }

    public String getSelectedDuration() {
        return selectedDuration;
    }

    public void setSelectedDuration(String selectedDuration) {
        this.selectedDuration = selectedDuration;
    }

    public SportFacilityBean getSportFacilityBean() {
        return sportFacilityBean;
    }

    public void setSportFacilityBean(SportFacilityBean sportFacilityBean) {
        this.sportFacilityBean = sportFacilityBean;
    }

    public boolean isRenderSportsList() {
        return renderSportsList;
    }

    public void setRenderSportsList(boolean renderSportsList) {
        this.renderSportsList = renderSportsList;
    }

    public String getSelectedEndTime() {
        return selectedEndTime;
    }

    public void setSelectedEndTime(String selectedEndTime) {
        this.selectedEndTime = selectedEndTime;
    }

    public int getSubPlayersNeeded() {
        return subPlayersNeeded;
    }

    public void setSubPlayersNeeded(int subPlayersNeeded) {
        this.subPlayersNeeded = subPlayersNeeded;
    }

    public int getSubPlayersToAdd() {
        return subPlayersToAdd;
    }

    public void setSubPlayersToAdd(int subPlayersToAdd) {
        this.subPlayersToAdd = subPlayersToAdd;
    }

    public SubPlayersService getSubPlayersService() {
        return subPlayersService;
    }

    public void setSubPlayersService(SubPlayersService subPlayersService) {
        this.subPlayersService = subPlayersService;
    }

    /**
     * @return the emailNotice
     */
    public EmailNoticeBean getEmailNotice() {
        return emailNotice;
    }

    /**
     * @param emailNotice the emailNotice to set
     */
    public void setEmailNotice(EmailNoticeBean emailNotice) {
        this.emailNotice = emailNotice;
    }

    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
