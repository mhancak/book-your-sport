/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import database.entities.ReservationE;
import database.service.ReservationService;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import utils.ReservationComparator;

/**
<<<<<<< HEAD
 * sdddddddd
 * @author mato
=======
 * dfgdfg dfgdfg
 * @author mato hancak
>>>>>>> druha_vetvicka
 */
public class ReservationLazyList extends LazyDataModel<ReservationE> {

    private static final long serialVersionUID = 1L;
    private List<ReservationE> reservations;
    private ReservationService reservationService;

    public ReservationLazyList(ReservationService reservationService) {
        super();
        this.reservationService = reservationService;
    }

    @Override
    public List<ReservationE> load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder, Map<String, String> filters) {

        //filter  
        if (!filters.isEmpty()) {
            for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                String filterProperty = it.next();
                String filterValue = filters.get(filterProperty);

                if (filterProperty.equals("title")) {
                    reservations = reservationService.getReservationsStartByTitle(filterValue);
                }
                else if (filterProperty.equals("sportFacility.name")) {
                    reservations = reservationService.getReservationsStartBySportFacilityName(filterValue);
                }
                else if (filterProperty.equals("sport.name")) {
                    reservations = reservationService.getReservationsStartBySportName(filterValue);
                }
                else if (filterProperty.equals("reserv.userE.email")) {
                    reservations = reservationService.getReservationsStartByUserEmail(filterValue);
                }
                else {
                    break;
                }

            }
        }
        else{
            reservations = reservationService.getReservations(startingAt, maxPerPage);
        }

        //usporiadanie
        if (sortField != null) {
            Collections.sort(reservations, new ReservationComparator(sortField, sortOrder));
        }

        //celkovy pocet rezervacii
        if (getRowCount() <= 0) {
            setRowCount(reservationService.countReservationsTotal().intValue());
        }

        //velkost strany
        setPageSize(maxPerPage);
        return reservations;
    }

    @Override
    public Object getRowKey(ReservationE reservation) {
        return reservation.getReservationID();
    }

    @Override
    public ReservationE getRowData(String reservationsID) {
        Long id = Long.valueOf(reservationsID);

        for (ReservationE reservation : this.reservations) {
            if (id.equals(reservation.getReservationID())) {
                return reservation;
            }
        }

        return null;
    }

    /**
     * @return the reservationService
     */
    public ReservationService getReservationService() {
        return reservationService;
    }

    /**
     * @param reservationService the reservationService to set
     */
    public void setReservationService(ReservationService reservationService) {
        this.reservationService = reservationService;
    }
}
