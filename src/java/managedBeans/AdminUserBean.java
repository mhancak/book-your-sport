/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import database.entities.UserE;
import database.service.UserService;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

/**
 *
 * @author mato
 */
@ManagedBean
@ViewScoped
public class AdminUserBean implements Serializable{
    
    @ManagedProperty(value = "#{userService}")
    private UserService userService;
    @ManagedProperty(value="#{language}")
    private LanguageBean languageBean;
    
    private String searchString;
    private UserE foundUser;
    private boolean renderPanelWithResults;
    
    private List<UserE> allAdmins;
    
    private ResourceBundle resBundle;
    private final static String LANG_FILE = "localization/web/administration";
    
    public void searchUser(){        
        this.foundUser = userService.getUserByEmail(this.searchString);
        if (this.foundUser == null){
            String rawMessage = getResBundle().getString("user.badid"); 
            String messageTxt = MessageFormat.format(rawMessage, searchString);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, messageTxt, "");  
            FacesContext.getCurrentInstance().addMessage(null, msg);          
        }        
    }
    
    public void changeAdminPermission(AjaxBehaviorEvent event){        
        //zmena opravneni v databaze
        userService.alterAdmin(this.foundUser.getId(), this.foundUser.isAdmin());
        
        FacesMessage msg;
        //vytvorenie spravy pre zobrazenie infa
        if (this.foundUser.isAdmin()){
            String rawMessage = getResBundle().getString("user.adminpermission.msg"); 
            String messageTxt = MessageFormat.format(rawMessage, this.foundUser.getEmail());
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, messageTxt, ""); 
        }
        else{
            String rawMessage = getResBundle().getString("user.normalpermission.msg"); 
            String messageTxt = MessageFormat.format(rawMessage, this.foundUser.getEmail());
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, messageTxt, ""); 
        }
         
        FacesContext.getCurrentInstance().addMessage(null, msg);  
    }
        
    /**
     * @return the userService
     */
    public UserService getUserService() {
        return userService;
    }

    /**
     * @param userService the userService to set
     */
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    /**
     * @return the searchString
     */
    public String getSearchString() {
        return searchString;
    }

    /**
     * @param searchString the searchString to set
     */
    public void setSearchString(String searchString) {
        this.searchString = searchString.trim();
    }

    /**
     * @return the foundUser
     */
    public UserE getFoundUser() {
        return foundUser;
    }

    /**
     * @param foundUser the foundUser to set
     */
    public void setFoundUser(UserE foundUser) {
        this.foundUser = foundUser;
    }

    /**
     * @return the renderPanelWithResults
     */
    public boolean isRenderPanelWithResults() {
        this.renderPanelWithResults = this.foundUser==null ? false : true;
        return renderPanelWithResults;
    }

  
    /**
     * @return the allAdmins
     */
    public List<UserE> getAllAdmins() {
        this.allAdmins = userService.getAllAdmins();
        return allAdmins;
    }

    /**
     * @param allAdmins the allAdmins to set
     */
    public void setAllAdmins(List<UserE> allAdmins) {
        this.allAdmins = allAdmins;
    }
    
     /**
     * @return the resBundle
     */
    public ResourceBundle getResBundle() {
        return resBundle;
    }

    /**
     * @return the languageBean
     */
    public LanguageBean getLanguageBean() {
        return languageBean;
    }

    /**
     * @param languageBean the languageBean to set
     */
    public void setLanguageBean(LanguageBean languageBean) {
        this.languageBean = languageBean;
        this.resBundle = ResourceBundle.getBundle(LANG_FILE, getLanguageBean().getLocale());
    }
}
