/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import database.entities.ReservationE;
import database.service.ReservationService;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.LazyDataModel;

/**
 * Manazovany bean pre spravu rezervacii v administracii.
 * 
 * @author mato hancak
 */
@ViewScoped
@ManagedBean
public class AdminReservations implements Serializable {

    private static final long serialVersionUID = 1L;
    /** Manazovany bean pre pristup k tabulke rezervacii v databaze */
    @ManagedProperty(value = "#{reservationService}")
    private ReservationService reservationService;
    /** Manazovany bean pre ziskanie lokalizacie */
    @ManagedProperty(value = "#{language}")
    private LanguageBean languageBean;
    /** Rezervacie ziskane z databazy na poziadavku */
    private LazyDataModel<ReservationE> reservations = null;
    /** Aktualna rezervacia  */
    private ReservationE currentReservation;

    /**
     * Metoda vrati vsetky rezervacie. Kedze rezervacii moze byt velky pocet,
     * jednotlive rezervacie sa vratia na poziadavku.
     *
     * @return the allReservations
     */
    public LazyDataModel<ReservationE> getAllReservations() {
        if (reservations == null) {
            reservations = new ReservationLazyList(reservationService);
        }

        return reservations;
    }
    
    /**
     * Odstranenie rezervacie z databazy a vypisanie potrebnych hlasok.
     */
    public void deleteReservation(){
        ResourceBundle resBundle = ResourceBundle.
                getBundle("localization/web/administration", getLanguageBean().getLocale());        
        FacesMessage msg;
        
        if (reservationService.deleteReservation(currentReservation.getReservationID())){
            String rawMessage = resBundle.getString("reservation.deletesuccess"); 
            String messageTxt = MessageFormat.format(rawMessage, currentReservation.getTitle());            
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, messageTxt, "");              
        }
        else{
            String rawMessage = resBundle.getString("reservation.deleteerror"); 
            String messageTxt = MessageFormat.format(rawMessage, currentReservation.getTitle());
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, messageTxt, "");    
        }
        FacesContext.getCurrentInstance().addMessage(null, msg); 
    }

    /**
     * @return the currentReservation
     */
    public ReservationE getCurrentReservation() {
        return currentReservation;
    }

    /**
     * @param currentReservation the currentReservation to set
     */
    public void setCurrentReservation(ReservationE currentReservation) {
        this.currentReservation = currentReservation;
    }

    /**
     * @return the reservationService
     */
    public ReservationService getReservationService() {
        return reservationService;
    }

    /**
     * @param reservationService the reservationService to set
     */
    public void setReservationService(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    /**
     * @return the languageBean
     */
    public LanguageBean getLanguageBean() {
        return languageBean;
    }

    /**
     * @param languageBean the languageBean to set
     */
    public void setLanguageBean(LanguageBean languageBean) {
        this.languageBean = languageBean;
    }
}
