/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service.impl;

import database.entities.ReservationE;
import database.entities.SportsE;
import database.entities.UserE;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mato
 */
public class UserBeanTest {
    private JPAResourceBean jPAResourceBean = new JPAResourceBean();
    
    public UserBeanTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetUserById() {
        //OK funguje
        System.out.println("getUserById");
        long id = 101;
        
        UserBean instance = new UserBean();
        instance.setJpaResourceBean(jPAResourceBean);
       
        UserE result = instance.getUserById(id);
        System.out.println("Pouzivatel: " + result);
    }
    
    @Test
    public void testGetUserByEmail() {
        //OK funguje
        System.out.println("getUserByEmail");
        
        UserBean instance = new UserBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        UserE user = instance.getUserById(103);
        
        System.out.println(user.getEmail());
    }
    
    @Test
    public void testCreateUser() {
        //OK funguje
        System.out.println("createUser");
        UserE user1 = new UserE("aaa@cvv.sk","mato", "novy","1234","0901234567", null, null);
               
        UserBean instance = new UserBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        instance.createUser(user1);
        
        UserE user2 = new UserE();      
        user2.setEmail("kobyla@x.sk");
        user2.setFirsname("mato");
        user2.setLastname("novy");
        user2.setPasswd("1234");
        user2.setPhone("0901234567");
        
        instance.createUser(user2);
        
        UserE user3 = new UserE();      
        user3.setEmail("rysula@x.sk");
        user3.setFirsname("mato");
        user3.setLastname("novy");
        user3.setPasswd("1234");
        user3.setPhone("0901234567");
        
        instance.createUser(user3);
        
        UserE user4 = new UserE();      
        user4.setEmail("pes@x.sk");
        user4.setFirsname("mato");
        user4.setLastname("novy");
        user4.setPasswd("1234");
        user4.setPhone("0901234567");
        
        instance.createUser(user4);
    }

    @Test
    public void testDeleteUser() {
        //OK funguje
        System.out.println("deleteUser");
        long userId = 52;
        UserBean instance = new UserBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        instance.deleteUser(userId);        
    }

    @Test
    public void testExistsEmail() {
        //OK funguje
        System.out.println("existEmail");
        
        String email = "pes@x.ssk";
        boolean expResult = false;
        
        UserBean instance = new UserBean();
        instance.setJpaResourceBean(jPAResourceBean);
                
        boolean result = instance.existsEmail(email);
        assertEquals(expResult, result);    
    }

    @Test
    public void testAlterEmail() {
        //OK funguje
        System.out.println("alterEmail");
        
        String newEmail = "oneee@lala.sk";
        UserBean instance = new UserBean();
        instance.setJpaResourceBean(jPAResourceBean);
        instance.alterEmail(102, newEmail);
    }

    @Test
    public void testAlterFirstname() {
        System.out.println("alterFirstname");
    }

    @Test
    public void testAlterLastname() {
        System.out.println("alterLastname");
    }

    @Test
    public void testAlterPass() {
        System.out.println("alterPass");
    }

    @Test
    public void testAlterPhone() {
        System.out.println("alterPhone");
    }

    @Test
    public void testAlterSports() {
        //OK funguje 
        System.out.println("alterSports");
        
        UserBean instance = new UserBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        SportsBean sb = new SportsBean();
        sb.setJpaResourceBean(jPAResourceBean);
        List<SportsE> sports = sb.getAllSports();
        
        instance.alterSports(102, sports);
    }

    @Test
    public void testAlterReservations() {
        System.out.println("alterReservations");
    }

    @Test
    public void testAlterUser() {
        System.out.println("alterUser");        
    }

    @Test
    public void testGetUsersBySportId() {
        //OK funguje
        System.out.println("getUsersBySportId");
                
        UserBean instance = new UserBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        List<UserE> user = instance.getUsersBySportId(1);
        
        System.out.println(user);
    }
    
    @Test
    public void testAlterIsAdmin() {
        //OK funguje
        System.out.println("alterIsAdmin");
        
        UserBean instance = new UserBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        UserE user = instance.getUserByEmail("rysula@x.sk");
        
        instance.alterAdmin(user.getId(), true);
    }
}
