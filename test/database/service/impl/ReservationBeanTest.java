/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service.impl;

import database.entities.ReservationE;
import database.entities.SportFacilityE;
import database.entities.SportsE;
import database.entities.SubPlayersE;
import database.entities.UserE;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author mato
 */
public class ReservationBeanTest {
    private JPAResourceBean jPAResourceBean;
    
    public ReservationBeanTest() {
        this.jPAResourceBean = new JPAResourceBean();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createReservation method, of class ReservationBean.
     */
    @Test
    public void testCreateReservation() {
        //OK funguje        
        System.out.println("createReservation");
        Calendar c = Calendar.getInstance();
        c.set(2012, 12, 11, 8, 0);
        Calendar e = Calendar.getInstance();
        e.set(2012, 12, 11, 10, 0);
        
        UserBean userBean = new UserBean();
        userBean.setJpaResourceBean(jPAResourceBean);
        UserE user = userBean.getUserById(101);
        
        SportsBean sportsBean = new SportsBean();
        sportsBean.setJpaResourceBean(jPAResourceBean);        
        SportsE sport = sportsBean.getSportById(1);
        
        SportsFacilitiesBean sportsFacilitiesBean = new SportsFacilitiesBean();
        sportsFacilitiesBean.setJpaResourceBean(jPAResourceBean);
        SportFacilityE sf = sportsFacilitiesBean.getSportFacilityById(2);
               
        ReservationE reservation1 = new ReservationE(user, sport, sf, "reserv2", c.getTime(), e.getTime());
        reservation1.setId("eventid1");
        ReservationE reservation2 = new ReservationE(user, sport, sf, "reserv1", c.getTime(), e.getTime());
        reservation2.setId("eventid2");
        
        ReservationBean instance = new ReservationBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        instance.createReservation(reservation1);
        instance.createReservation(reservation2);
    }

    /**
     * Test of deleteReservation method, of class ReservationBean.
     */
    @Test
    public void testDeleteReservation() {
        //OK funguje
        System.out.println("deleteReservation");
        long reservationId = 701;
        ReservationBean instance = new ReservationBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        instance.deleteReservation(reservationId);
    }

    /**
     * Test of getReservationById method, of class ReservationBean.
     */
    @Test
    public void testGetReservationById() {
        //OK funguje
        System.out.println("getReservationById");
        long reservationId = 751L;
        ReservationBean instance = new ReservationBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        ReservationE result = instance.getReservationById(reservationId);
        System.out.println(result);
    }

    /**
     * Test of getReservationsByResDate method, of class ReservationBean.
     */
    @Test
    public void testGetReservationsByResDate() {
        //OK funguje
        System.out.println("getReservationsByResDate");
        
        Calendar c = Calendar.getInstance();
        c.set(2012, 11, 19);
        Date date = c.getTime();
        
        ReservationBean instance = new ReservationBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        List<ReservationE> result = instance.getReservationsByResDate(date);
        System.out.println("LIST: "+result.size());
    }

    /**
     * Test of alterReqPlayers method, of class ReservationBean.
     */
    @Test
    public void testAlterReqPlayers() {
        //OK funguje
        System.out.println("alterReqPlayers");
        long reservationId = 751;
        int newReqPlayers = 5;
        
        ReservationBean instance = new ReservationBean();
        instance.setJpaResourceBean(jPAResourceBean);
                
        instance.alterReqPlayers(reservationId, newReqPlayers);
    }

    /**
     * Test of alterStartTime method, of class ReservationBean.
     */
    @Test
    public void testAlterStartTime() {
        //OK funguje
        System.out.println("alterStartTime");
        long reservationId = 751;
        Calendar c = Calendar.getInstance();
        c.set(2012, 12, 23);
        Date newStartTime = c.getTime();
        
        ReservationBean instance = new ReservationBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        instance.alterStartTime(reservationId, newStartTime);
    }

    /**
     * Test of alterEndTime method, of class ReservationBean.
     */
    @Test
    public void testAlterEndTime() {
        //OK funguje
        System.out.println("alterEndTime");
        long reservationId = 751;
        Calendar c = Calendar.getInstance();
        c.set(2012, 12, 31);
        Date newEndTime = c.getTime();
        
        ReservationBean instance = new ReservationBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        instance.alterEndTime(reservationId, newEndTime);
    }

    /**
     * Test of alterReservDate method, of class ReservationBean.
     */
    @Test
    public void testAlterReservDate() {
        //OK funguje
        System.out.println("alterReservDate");
        long reservationId = 751;
        Calendar c = Calendar.getInstance();
        c.set(2012, 12, 31);
        Date newReservDate = c.getTime();
        
        ReservationBean instance = new ReservationBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        instance.alterReservDate(reservationId, newReservDate);
    }

    /**
     * Test of alterSubPlayer method, of class ReservationBean.
     */
    @Test
    public void testAlterSubPlayer() {
        //OK funguje
        System.out.println("alterSubPlayer");
        long reservationId = 751;
        
        SubPlayersBean spb = new SubPlayersBean();
        spb.setJpaResourceBean(jPAResourceBean);
        SubPlayersE sp = spb.getSubPlayersById(151);        
        
        ReservationBean instance = new ReservationBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        ReservationE re = instance.getReservationById(reservationId);
        List<SubPlayersE> newSubPlayers = re.getSubPlayers();
        if (newSubPlayers == null){
            newSubPlayers = new ArrayList<SubPlayersE>();            
        }
        newSubPlayers.add(sp);
        
        instance.alterSubPlayer(reservationId, newSubPlayers);
    }

    /**
     * Test of getReservationsBySportId method, of class ReservationBean.
     */
    @Test
    public void testGetReservationsBySportId() {
        //OK funguje
        System.out.println("getReservationsBySportId");
        long sportId = 5;
        
        ReservationBean instance = new ReservationBean();
        instance.setJpaResourceBean(jPAResourceBean);
                
        List result = instance.getReservationsBySportId(sportId);
        System.out.println(result);        
    }

    @Test
    public void testGetReservationByEventID() {
        System.out.println("getReservationByEventID");
        String eventID = "eventid1";
        
        ReservationBean instance = new ReservationBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        ReservationE result = instance.getReservationByEventID(eventID);
        System.out.println(result);
    }
   
}
