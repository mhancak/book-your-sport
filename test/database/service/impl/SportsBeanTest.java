/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service.impl;

import database.entities.SportsE;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mato
 */
public class SportsBeanTest {
    private JPAResourceBean jPAResourceBean;
    
    public SportsBeanTest() {
        this.jPAResourceBean = new JPAResourceBean();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createSport method, of class SportsBean.
     */
    @Test
    public void testCreateSport() {
        //OK funguje
        System.out.println("createSport");
        SportsE sport1 = new SportsE("Vybíjaná");
        SportsE sport2 = new SportsE("Futbal");
        SportsE sport3 = new SportsE("Hokej");
        SportsE sport4 = new SportsE("Tenis");
        SportsE sport5 = new SportsE("Basketbal");
        
        SportsBean instance = new SportsBean();
        instance.setJpaResourceBean(jPAResourceBean);
        instance.createSport(sport1);
        instance.createSport(sport2);
        instance.createSport(sport3);
        instance.createSport(sport4);
        instance.createSport(sport5);
    }

    /**
     * Test of deleteSport method, of class SportsBean.
     */
    @Test
    public void testDeleteSport() {
        //OK funguje
        System.out.println("deleteSport");
        long sportId = 651L;
        SportsBean instance = new SportsBean();
        instance.setJpaResourceBean(jPAResourceBean);
        instance.deleteSport(sportId);
    }

    /**
     * Test of getSportById method, of class SportsBean.
     */
    @Test
    public void testGetSportById() {
        //OK funguje
        System.out.println("getSportById");
        long sportId = 4;
        
        SportsBean instance = new SportsBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        SportsE result = instance.getSportById(sportId);
        System.out.println(result.getName());
    }

    /**
     * Test of getAllSports method, of class SportsBean.
     */
    @Test
    public void testGetAllSports() {
        //OK funguje
        System.out.println("getAllSports");
        SportsBean instance = new SportsBean();
        instance.setJpaResourceBean(jPAResourceBean);
            
        List result = instance.getAllSports();
        System.out.println(result);
    }

    /**
     * Test of alterSportName method, of class SportsBean.
     */
    @Test
    public void testAlterSportName() {
        //OK funguje
        System.out.println("alterSportName");
        long sportId = 4;
        String newName = "Badminton";
        SportsBean instance = new SportsBean();
        instance.setJpaResourceBean(jPAResourceBean);
        instance.alterSportName(sportId, newName);
    }

   
}
