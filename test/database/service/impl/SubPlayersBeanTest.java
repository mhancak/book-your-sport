/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package database.service.impl;

import database.entities.SubPlayersE;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mato
 */
public class SubPlayersBeanTest {
    private JPAResourceBean jPAResourceBean;
    
    
    public SubPlayersBeanTest() {
        jPAResourceBean = new JPAResourceBean();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetSubPlayersById() {
        //OK funguje
        System.out.println("getSubPlayersById");
        long subPlayerId = 1;
        
        SubPlayersBean instance = new SubPlayersBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        SubPlayersE result = instance.getSubPlayersById(subPlayerId);
        System.out.println("Vysledok: " + result);
    }

    @Test
    public void testCreateSubPlayers() {
        //OK funguje
        System.out.println("createSubPlayers");
        
        SubPlayersE newSubPlayer = new SubPlayersE();
        newSubPlayer.setCount(12);
        
        SubPlayersBean instance = new SubPlayersBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        instance.createSubPlayers(newSubPlayer);
    }

    @Test
    public void testDeleteSubPlayers() {
        //OK funguje 
        System.out.println("deleteSubPlayers");
        
        long subPlayerId = 2;
        
        SubPlayersBean instance = new SubPlayersBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        instance.deleteSubPlayers(subPlayerId);
    }

    @Test
    public void testAlterSubPlayersCount() {
        //OK funguje
        System.out.println("alterSubPlayersCount");
        long subPlayerId = 1L;
        int newCount =99;
        
        SubPlayersBean instance = new SubPlayersBean();
        instance.setJpaResourceBean(jPAResourceBean);
        
        instance.alterSubPlayersCount(subPlayerId, newCount);
    }

}
